import "./index.scss";

import Card from "./components/Card";
import Header from "host/src/components/Header";
import React from "react";
import ReactDOM from "react-dom";

const App = () => (
  <div className="h-screen bg-yellow-200">
    <Header title="Remote" />
    <div className="flex justify-center items-center p-20">
      <Card />
    </div>
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
