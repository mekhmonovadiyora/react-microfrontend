export default function Card() {
  return (
    <div className="flex flex-col gap-4 bg-white shadow-lg rounded-2xl w-96 h-96">
      <div className="bg-black p-4 w-full rounded-t-2xl flex flex-row justify-between">
        <h1 className="text-white text-2xl font-semibold">Product A</h1>
        <p className="text-white text-sm">remote component</p>
      </div>
      <div className="p-4">
        <p className="font-medium text-lg">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Quisquam
          voluptatum, quibusdam, quia, quae voluptates voluptatibus consequuntur
          voluptate quod quos doloribus quas. Quisquam voluptatum, quibusdam,
          quia, quae voluptates voluptatibus consequuntur voluptate quod quos
          doloribus quas.
        </p>
      </div>
    </div>
  );
}
