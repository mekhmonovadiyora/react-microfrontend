export default function Header({ title }) {
  return (
    <header className="flex justify-between items-center bg-black h-20 text-white p-4">
      <div className="text-3xl">{title}</div>

      <p className="text-md">shared header</p>
    </header>
  );
}
