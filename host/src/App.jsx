import "./index.scss";

import Card from "remote/src/components/Card";
import Header from "./components/Header";
import React from "react";
import ReactDOM from "react-dom";

const App = () => (
  <div className="h-screen bg-gray-100">
    <Header title="Host" />
    <div className="flex justify-center items-center p-20">
      <Card />
    </div>
  </div>
);
ReactDOM.render(<App />, document.getElementById("app"));
